class Fixnum
  def in_words
    arr_stored = self.to_s.chars.map(&:to_i)
    #return arr_stored
    if self == 0
      return 'zero'
    end
    flag = 1
    ans = ''

    while flag*3 < arr_stored.length
      #there is something to write
      if arr_stored[-(flag*3)..-(flag*3-2)].join != '000'
        if flag == 1
          ans << hundreds(arr_stored[-(flag*3)..-(flag*3-2)])
        elsif flag == 2
          temp_ans = hundreds(arr_stored[-(flag*3)..-(flag*3-2)]) \
          + ' thousand'
          if ans != ''
            ans = temp_ans + ' ' + ans
          end
        elsif flag == 3
          temp_ans = hundreds(arr_stored[-(flag*3)..-(flag*3-2)]) \
          + ' million'
          if ans != ''
            ans = temp_ans + ' ' + ans
          end
        elsif flag == 4
          temp_ans = hundreds(arr_stored[-(flag*3)..-(flag*3-2)]) \
          + ' billion'
          if ans != ''
            ans = temp_ans + ' ' + ans
          end
        elsif flag == 5
          temp_ans = hundreds(arr_stored[-(flag*3)..-(flag*3-2)]) \
          + ' trillion'
          if ans != ''
            ans = temp_ans + ' ' + ans
          end
        end
      end
      flag += 1
    end
    tempans = ''
    if flag == 5
      tempans = hundreds(arr_stored[0..-(flag*3-2)]) + ' trillion'
    elsif flag == 4
      tempans = hundreds(arr_stored[0..-(flag*3-2)]) + ' billion'
    elsif flag == 3
      tempans = hundreds(arr_stored[0..-(flag*3-2)]) + ' million'
    elsif flag == 2
      tempans = hundreds(arr_stored[0..-(flag*3-2)]) + ' thousand'
    else
      return hundreds(arr_stored[0..-(flag*3-2)])
    end
    if ans != ''
      tempans + ' ' + ans
    else
      tempans
    end
  end

  def hundreds(arr_of_three)
    ans = ''
    if arr_of_three.length == 3
      if arr_of_three[0] != 0 && (arr_of_three[1] != 0 || arr_of_three[2] != 0)
        ans << dig_to_str(arr_of_three[0])+' hundred'
        ans << ' ' + tens_and_less(arr_of_three[1..2])
      elsif arr_of_three[0] != 0
        ans << dig_to_str(arr_of_three[0])+' hundred'
      else
        ans << tens_and_less(arr_of_three[1..2])
      end
    elsif arr_of_three.length == 2
      ans << tens_and_less(arr_of_three)
    else
      ans << dig_to_str(arr_of_three[0])
    end
    ans
  end

  def tens_and_less(arr_of_two)
    #special cases
    if arr_of_two[0] == 1
      return teens(arr_of_two)
    elsif arr_of_two[0] == 0
      return dig_to_str(arr_of_two[1])
    else
      ans = ""
      if arr_of_two[0] == 2
        ans << "twenty"
      elsif arr_of_two[0] == 3
        ans << "thirty"
      elsif arr_of_two[0] == 4
        ans << "forty"
      elsif arr_of_two[0] == 5
        ans << "fifty"
      elsif arr_of_two[0] == 6
        ans << "sixty"
      elsif arr_of_two[0] == 7
        ans << "seventy"
      elsif arr_of_two[0] == 8
        ans << "eighty"
      elsif arr_of_two[0] == 9
        ans << "ninety"
      end
      if arr_of_two[1] != 0
        ans << " " + dig_to_str(arr_of_two[1])
      end
    end
    ans

  end

  def teens(arr_of_two)
    joined = arr_of_two.join
    if joined == "10"
      return "ten"
    elsif joined == "11"
      return "eleven"
    elsif joined == "12"
      return "twelve"
    elsif joined == "13"
      return "thirteen"
    elsif joined == "14"
      return "fourteen"
    elsif joined == "15"
      return "fifteen"
    elsif joined == "16"
      return "sixteen"
    elsif joined == "17"
      return "seventeen"
    elsif joined == "18"
      return "eighteen"
    elsif joined == "19"
      return "nineteen"
    end
  end


  def dig_to_str(digit)
    if digit == 1
      "one"
    elsif digit == 2
      "two"
    elsif digit == 3
      return 'three'
    elsif digit == 4
      return 'four'
    elsif digit == 5
      return 'five'
    elsif digit == 6
      return 'six'
    elsif digit == 7
      return 'seven'
    elsif digit == 8
      return 'eight'
    elsif digit == 9
      return 'nine'
    end
  end

end
